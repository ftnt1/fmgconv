#!/usr/bin/env python3

"""fmgconv.py: It converts FortiManager / FortiAnalyzer backup file to VM format while removing admin passwords,
 trusted hosts, shell password, password policy and non-standard ports."""

__author__ = "Lukasz Korbasiewicz"
__credits__ = ["Andy Mills", "Henry Zheng", "Deborah Geisau"]
__version__ = "1.4.2"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__status__ = "Production"

import os
import sys
import tarfile
import fileinput
import re


def filelist():
    return os.listdir("./")


def varchecker():
    for file in filelist():
        if file == "var":
            input("\n\n\n'var' folder detected in run directory - please remove it before proceeding "
                  "to avoid mess with your new converted backup file.\n\n\n"
                  "Press Enter to exit...\n\n\n")
            sys.exit(0)


def datprint():
    datfileindex = 0
    for file in filelist():
        if ".dat" in file:
            print(str(datfileindex) + " - " + file)
            datfileindex += 1
    if datfileindex < 1:
        input("There are no files with .dat extension detected."
              "Run this script from a directory containing at least one .dat file.\n\n"
              "Press Enter to exit...\n\n\n")
        sys.exit(0)


def filetable():
    # Create empty filelist
    datfiles = []
    datfileindex = 0

    # This will add all .dat files to list "datfilelist"
    for file in filelist():
        if ".dat" in file:
            datfiles.append(file)
            datfileindex += 1
    return datfiles


def filepicker():
    datfileindex = input("\nWhich file you want to convert? Type number only!  ")
    datfileindex = int(datfileindex)
    return filetable()[datfileindex]


def unpacker():
    print("\n********************************************************************************\n"
          "Unpacking file " + datfilename +
          "\n********************************************************************************")
    tarfile.open(datfilename, "r|gz").extractall()
    tarfile.open(datfilename, "r|gz").close()
    print("Done!\n")


def headerchanger():
    print("\n********************************************************************************\n"
          "Changing old header in ./var/fwclienttemp/system.conf\n"
          "********************************************************************************\n" +
          filetolist[0])
    filetolist[0] = re.sub(r'FMG(.+)-4', "FMG-VM64-4", filetolist[0])
    filetolist[0] = re.sub(r'FMG(.+)-5', "FMG-VM64-5", filetolist[0])
    filetolist[0] = re.sub(r'FMG(.+)-6', "FMG-VM64-5", filetolist[0])
    filetolist[0] = re.sub(r'FAZ(.+)-4', "FAZVM64-4", filetolist[0])
    filetolist[0] = re.sub(r'FAZ(.+)-5', "FAZVM64-5", filetolist[0])
    filetolist[0] = re.sub(r'FAZ(.+)-6', "FAZVM64-5", filetolist[0])
    filetolist[0] = re.sub(r'FL(.+)-4', "FAZVM64-4", filetolist[0])
    filetolist[0] = re.sub(r'FL(.+)-5', "FAZVM64-5", filetolist[0])
    filetolist[0] = re.sub(r'FL(.+)-6', "FAZVM64-5", filetolist[0])
    print("\n********************************************************************************\n"
          "New header:\n"
          "********************************************************************************\n" +
          filetolist[0])


def admpassremover():
    print("\n********************************************************************************\n"
          "Removing admin passwords\n"
          "********************************************************************************")
    for position, item in enumerate(filetolist[adminstart:adminend:]):
        if item.startswith("        set password"):
            filetolist[adminstart + position] = ""
    print("Done!\n")


def trusthostremover():
    print("\n********************************************************************************\n"
          "Removing trusted hosts\n"
          "********************************************************************************")
    for position, item in enumerate(filetolist[adminstart:adminend:]):
        if item.startswith("        set trusthost") or item.startswith("        set ipv6_trusthost"):
            filetolist[adminstart + position] = ""
    print("Done!\n")


def shellpassremover():
    print("\n********************************************************************************\n"
          "Removing FMG Shell password\n"
          "********************************************************************************")
    for position, item in enumerate(filetolist[adminsettingstart:adminsettingend:]):
        if item.startswith("    set shell-password"):
            filetolist[adminsettingstart + position] = ""
    print("Done!\n")


def customportremover():
    print("\n********************************************************************************\n"
          "Removing custom ports\n"
          "********************************************************************************")
    for position, item in enumerate(filetolist[adminsettingstart:adminsettingend:]):
        if item.startswith("    set https_port") or item.startswith("    set http_port"):
            filetolist[adminsettingstart + position] = ""
    print("Done!\n")


def passpolicyremover():
    print("\n********************************************************************************\n"
          "Removing password policy\n"
          "********************************************************************************")
    adminsettingstart = filetolist.index("config system password-policy\n")
    adminsettingend = filetolist[adminsettingstart::].index("end\n") + adminsettingstart
    for position, item in enumerate(filetolist[adminsettingstart:adminsettingend:]):
        if item.startswith("    set"):
            filetolist[adminsettingstart + position] = ""
    print("Done!\n")


def rpcpermitrwsetter():
    print("\n********************************************************************************\n"
          "Feature Request by Deborah:add 'set rpc-permit read-write' to admins\n"
          "Don't look at the code - it's ugly and can burn your eyes\n"
          "YOU HAVE BEEN WARNED!!!"
          "\n********************************************************************************")

    admusers = filetolist[adminstart:adminend:]
    admuserstart = [admin for admin, l in enumerate(admusers) if l.startswith('    edit ')]
    admuserstart = [admuser + adminstart + 1 for admuser in admuserstart]
    admuserend = [admin for admin, l in enumerate(admusers) if l.startswith('    next')]
    admuserend = [admuser + adminstart + 1 for admuser in admuserend]
    n = 0
    li = 0
    for admin in admuserstart:
        if "        set rpc-permit read-write\n" not in filetolist[admuserstart[n] + li:admuserend[n] + li:]:
            filetolist.insert(admuserstart[n] + li, "        set rpc-permit read-write\n")
            li += 1
            n += 1
        else:
            for position, item in enumerate(filetolist[admuserstart[n] + li:admuserend[n] + li:]):
                if item.startswith("        set rpc-permit"):
                    filetolist[admuserstart[n] + position] = "        set rpc-permit read-write\n"
            n += 1
    print("Done!\n")


def confsaver():
    print("\n********************************************************************************\n"
          "Saving changes to ./var/fwclienttemp/system.conf\n"
          "********************************************************************************")
    for element in filetolist:
        tempsysconf.write(element)
    print("Done!\n")
    print("\n********************************************************************************\n"
          "Closing ./var/fwclienttemp/system.conf\n"
          "********************************************************************************")
    tempsysconf.close()
    print("Done!\n")


def repacker():
    print("\n********************************************************************************\n"
          "Packing file " + shortfile +
          "_converted.dat\n********************************************************************************")
    newfile = tarfile.open(converted, "w|gz")
    newfile.add("var/fwclienttemp/system.conf")
    newfile.add("var")
    newfile.close()
    tempsysconf.close()
    print("Done!\n")


try:
    varchecker()
    datprint()
    datfilename = filepicker()
    shortfile = re.sub(r'\.dat$', '', datfilename)
    converted = "converted_" + shortfile + ".dat"

    unpacker()

    systemconf = "./var/fwclienttemp/system.conf"
    filetolist = []

    tempsysconf = open(systemconf, 'r+')
    for line in fileinput.input(systemconf):
        filetolist.append(line)
    print(fileinput.input(systemconf)[0])

    headerchanger()

    adminstart = filetolist.index("config system admin user\n")
    adminend = filetolist[adminstart::].index("end\n") + adminstart
    admpassremover()
    trusthostremover()

    adminsettingstart = filetolist.index("config system admin setting\n")
    adminsettingend = filetolist[adminsettingstart::].index("end\n") + adminsettingstart
    shellpassremover()
    customportremover()
    passpolicyremover()
    rpcpermitrwsetter()

    confsaver()
    repacker()

    input("Press Enter to exit...\n\n\n")
    sys.exit(0)

except Exception as ex:
    print(ex)
    input("\n\nError! Press ENTER to exit...\n")
    sys.exit(0)
