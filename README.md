# README #

FortiManager and FortiAnalyzer backup file converter

## Files ##

### fmgconv.py ###

It converts FortiManager backup file to FMG-VM64 format while removing all admin passwords, trusted hosts, exe shell password, password policy and non-standard ports
It also adds 'set rpc-permit read-write' to admins to allow API usage

### README.md ###

This file

## Usage ##
Run this script from a directory containing unencrypted FortiManager or FortiAnalyzer backup file.
The file extension has to be .dat