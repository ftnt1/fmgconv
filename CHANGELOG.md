# Changelog: #

## 1.4.2 ##

### Added ###

- Now script can finally correctly convert FortiManager and FortiAnalyzer v6
  headers. It also correctly recognizes FAZ backups where model starts with FL
  now FAZ
## 1.4.0 ##

### Changed ###

- Major code refactor to easier maintain and extend script features

## 1.3.6 ##

### Fixed ###

- It will not exit on error – it will display error and wait for you to press enter before closing. Never again disappearing command line window when there is some issue. Handy for troubleshooting any potential issues.
- In fact it doesn't exit after finishing it's job too, so you can see what was happening (old/new header etc)

## 1.3.5 ##

### Added ###
- NFR for Deborah - add 'set rpc-permit read-write' to admin users
- It will now warn you if you have old “var” directory and won’t continue until you remove it to avoid messy converted backup file

### Removed ###
- It doesn't try to remove old 'var' folder any more as it wasn't working properly on some Windows machines anyway

## 1.3 ##

### Added: ###

- it now removes password policy and non-standard ports

### Changed ###

- Now it  removes password from all admin users not only “admin” so even if “admin” was deleted you can still log in

## 1.0 ##
Initial release
